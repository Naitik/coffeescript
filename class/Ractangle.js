// Generated by CoffeeScript 1.4.0
(function() {
  var Ractangle, r;

  Ractangle = (function() {

    function Ractangle(b, h) {
      this.b = b;
      this.h = h;
    }

    Ractangle.prototype.area = function() {
      return this.b * this.h;
    };

    Ractangle.prototype.perimeter = function() {
      return 2 * this.b + 2 * this.h;
    };

    return Ractangle;

  })();

  r = new Ractangle(2, 3);

  console.log(r.area());

  console.log(r.perimeter());

}).call(this);

class Circle
  constructor : (@r) ->

  area :-> 3.14 * @r * @r
  perimeter : -> 2 * 3.14 * @r

c = new Circle(3)
console.log(c.area())
console.log(c.perimeter())
express = require('express')
http = require('http')

app = express()

app.configure(() ->
  app.set('port', process.env.PORT || 3000)
  app.set('views', __dirname + '/views')
  app.set('view engine', 'jade')

  app.use(express.bodyParser())
  app.use(express.methodOverride())
  app.use(app.router)
  app.use(express.static(__dirname + "/public"))
);
###
app.get '/', (req, res) ->
  res.render 'home', { title : "Building node application using express" }
###

###app.get '/hi', (req, res)->
  message = [
    "<h1>Hello World</h1>"
    "<p>Welcome to building Mode.js app</p>"
    "<ul><li>fast</li>"
    "<li>fan</li></ul>"].join("\n")
  res.send message

app.get '/users/:userId', (req, res) ->
  res.send "Hello , user #" + req.params.userId  + "!"

app.post '/user', (req, res) ->
  res.send "Creating a new user with the name " + req.body.username +"."###

users = [ 'nik' , 'for' , 'fun', 'soni', 'is', 'here']

app.param 'from', (req, res, next, from) ->
  req.form = parseInt(from, 10)
  next()
  return

app.param 'to', (req, res, next, to)->
  req.to = parseInt(to, 10)
  next()
  return

app.get '/users/:from-:to', (req, res) ->
  from = parseInt(req.parse.from, 10)
  to = parseInt(req.parse.to, 10)
  res.json(users.slice(req.from, req.to + 1))



http.createServer(app).listen app.get('port'), () ->
  console.log('Express Server Listening on port' + app.get('port'))

fals

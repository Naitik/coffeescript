Queue  = ->
  node = (data_, next_) ->
    data : -> data_
    next : -> next_

  top_ = null
  start_ = null
  count_ = 0

  return {
  enqueue : (ele) ->
    top_ = node(ele, top_)
    ++count_
    if start_ is null and count_ is 1
      start_ = node(ele,top_)

  dequeue : (ele) ->
    result = start_.data()
    start_ = start_.next()
    --count_

  isEmpty : ->
    if top_ == null
      return true
    false

  size : ->
    count_
  }
Stack = ->
  node = (data_, next_)->
    data : -> data_
    next : -> next_

  top_ = null
  count_ = 0

  return {
  push : (ele) ->
    top_ = node(ele, top_)
    ++count_

  pop : (ele) ->
    result = top_.data()
    top_ = top_.next()
    --count_
    result

  top : ->
    top_.data()

  isEmpty : ->
    if top_ == null
      return true
    false

  size : ->
    count_

  }
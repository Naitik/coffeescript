mergeSort = (arr) ->
    mid = Math.floor(arr.length / 2)
    left = arr.slice(0,mid)
    right = arr.slice(mid)

    if arr.length is 1
       return arr
    merge mergeSort(left), mergeSort(right)

merge=(left, right) ->
    result = []
    while left.length or right.length
      if left.length && right.length
        if left[0] < right[0]
          result.push(left.shift())
        else result.push(right.shift())

      else if left.length
        result.push(left.shift())

      else
        result.push(right.shift())
    result

console.log(mergeSort [1,5,9,8,455,0,2,3,4])


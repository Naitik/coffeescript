josephus = (n, interval) ->
  r = 0
  d = 2
  while d <= n
    r = (r + interval) % d
    d = d + 1
  r + 1
console.log(josephus(5,2))
console.log(josephus(5,5))


